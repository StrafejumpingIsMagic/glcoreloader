/*
Copyright 2018 Jessica Allan

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>
#include <regex>
#include <cstdio>

std::string string_uppercase(const std::string& in_s) {
	std::string t{in_s};
	for (char & c : t) {
		if (c >= 97 && c <= 122) c -= 32;
	}
	return t;
}

std::string string_quote(const std::string& in_s) {
	return std::string("\"") + in_s + std::string("\"");
}

std::string string_rmglpre(const std::string& s) {
	return ( (s.length() > 2) && s[0] == 'g' && s[1] == 'l' ) ? s.substr(2) : s;
}

int main(const int argc, const char ** argv) {
	bool opt_ext = false, opt_global = false, opt_lib = false, opt_inl = false;
	std::string opt_glcorearb_h = "/usr/include/GL/glcorearb.h";
	std::string opt_outfile, libheadername;
	bool output_stdout = true;
	const std::string arg_str_ext{"--ext"};
	const std::string arg_str_global{"--global"};
	const std::string arg_str_infile{"--input"};
	const std::string arg_str_outfile{"--output"};
	const std::string arg_str_inl{"--inl"};
	const std::string arg_str_lib{"--library"};
	const std::string arg_str_libhn{"--library-header"};
	const std::string arg_str_help{"--help"};
	for (int i = 1; i < argc; i++) {
		if (arg_str_help.compare(argv[i]) == 0) {
			std::cout << "\nUsage: " << argv[0] << " [--help] [--ext] [--global] [--input <path>] [--output <path>] [--inl] [--library] [--library-header <name>]" << R"EOM(

The default when don't give the --inl or --library flags is to generate a header which optionally includes the implementation code depending on GLCORELOADER_INLINE or INLINE_MODULES macros.

Generally you will want at least the implementation and header, if aren't inlining everything then also a library.

The makefile provided lets you select specific versions to build and handles the dependencies between generated files for you. At a minimim, include the makefile in your project's makefile then add $(GLCORELOADER) as an include path, then depend on $(GLCORELOADER)glcoreloader.h.

Arguments:

	--help

		Shows this text then exits.
	
	--ext

		Include extensions among the function pointers to import.
	
	--global

		Keep a global state and define macros to use it like normal OpenGL calls. Multiple header files for this global state variant will conflict, don't use multiple together.
	
	--input <path>

		Path to the glcorearb.h file want to use.
	
	--output <path>

		File to write to.

	--inl

		Output the implementation code, using macros which change depending on the usage context. This is the only case don't need to provide a library header name with --library-header.

		In the context of an inline-library the macros expand to inlining functions and global definitions.

		In the context of a library build the macros expand to nothing, defining everything once.

		In the context of inclusion of the header file while not defining macros to state glcoreloader should be inlined, it is assumed a library will be included at link time so the implementation file isn't included.
	
	--library

		Output library code, which depends on implementation code.
	
	--library-header <name>

		Library header to use, required if aren't using --inl.

		This is the full name of the header, including extension.
	
)EOM";
			return 0;
		} else if (arg_str_ext.compare(argv[i]) == 0) {
			opt_ext = true;
		} else if (arg_str_global.compare(argv[i]) == 0) {
			opt_global = true;
		} else if (arg_str_inl.compare(argv[i]) == 0) {
			opt_inl = true;
		} else if (arg_str_lib.compare(argv[i]) == 0) {
			opt_lib = true;
		} else if (arg_str_libhn.compare(argv[i]) == 0) {
			i++; if (i < argc) libheadername = argv[i];
		} else if (arg_str_infile.compare(argv[i]) == 0) {
			i++; if (i < argc) opt_glcorearb_h = argv[i];
		} else if (arg_str_outfile.compare(argv[i]) == 0) {
			i++; if (i < argc) {
				opt_outfile = argv[i];
				output_stdout = false;
			}
		} else {
			//opt_glcorearb_h = argv[i];
			//curse this, created such a confusing bug
			std::cerr << "Unrecognised argument " << string_quote(argv[i]) << "\n";
			return -6;
		}
	}
	FILE * glcorearb_h_file = fopen(opt_glcorearb_h.c_str(), "rb");
	if (!glcorearb_h_file) {
		std::cerr << "Failed to open input file " << string_quote(opt_glcorearb_h) << std::endl;
		return -1;
	}

	std::string file_text;

	char buf[8192];
	size_t r = fread(&buf[0], 1, 8192, glcorearb_h_file);
	while (r) {
		file_text.append(&buf[0], r);
		r = fread(&buf[0], 1, 8192, glcorearb_h_file);
	}

	fclose(glcorearb_h_file);

	std::vector<std::string> gl_function_name_list;
	std::string cline;

	const char * callmatcher_regex = ".*GLAPI.*APIENTRY +([a-zA-Z0-9_]+).*";
	const char * function_ext_matcher_regex = ".*(ARB|EXT|KHR|OVR|NV|AMD|INTEL)$";

	std::regex callmatcher{callmatcher_regex, std::regex::extended};
	std::regex extmatcher{function_ext_matcher_regex, std::regex::extended};

	std::function<void(const std::string&)> parse_line = [&](const std::string& l) {
		std::smatch matches;
		if ( std::regex_match(l, matches, callmatcher) ) {
			if (matches.size() == 2) {
				if (!opt_ext) {
					if ( std::regex_match(matches[1].str(), extmatcher) ) {
						return;
					}
				}
				gl_function_name_list.push_back(matches[1]);
			}
		}
	};

	for (char& c : file_text) {
		switch (c) {
			case 10:
			case 13:
				parse_line(cline);
				cline = "";
				break;
			default:
				cline.append(1, c);
		}
	}
	if ( cline.length() ) parse_line(cline);

	std::ostream * strout = &std::cout;
	std::ofstream * outstream_ptr = nullptr;

	if (!output_stdout) {
		outstream_ptr = new std::ofstream(opt_outfile);
		if ( outstream_ptr->fail() ) {
			std::cerr << "Failed to open output file " << string_quote(opt_outfile) << std::endl;
			return -1;
		}
		strout = outstream_ptr;
	};

	if ( !gl_function_name_list.size() ) {
		// filter out potentially nasty characters for terminals
		for (char & c : file_text) {
			if (c < 32 || c == 127) c = '_';
		}
		std::cerr << "No functions found, this shouldn't happen.\n";
		std::cerr << "Read file contents:\n";
		std::cerr << file_text << "\n";
		return -24;
	}

*strout << R"EOM(#ifndef __cplusplus
#error Requires C++
#endif
)EOM";

	if (opt_global) {
*strout << R"EOM(#if __cplusplus < 201703L
#error Requires C++17 compatible compiler
#endif
)EOM";
	} else {
*strout << R"EOM(#if __cplusplus < 201103L
#error Requires C++11 compatible compiler
#endif
)EOM";
	}

	std::string cn = "gl_core";
	if (opt_ext) cn.append("_ext");
	if (opt_global) cn.append("_gs");

	if ( !libheadername.length() ) {
		libheadername = "glcoreloader";
		if (opt_ext) libheadername.append("_ext");
		if (opt_global) libheadername.append("_g");
		libheadername.append(".h");
	}
	
	std::string fq = cn + std::string("_fq"),
		vq = cn + std::string("_vq"),
		vdi = cn + std::string("_vdi"),
		fpre = cn + std::string("_proc_state_t::");

	if (!opt_lib && !opt_inl) {

	*strout <<
		"#ifndef __" << cn << "_c_h\n" <<
		"#define __" << cn << "_c_h\n" <<
		R"EOM(#include <GL/glcorearb.h>
#ifndef __gl_h_
#define __gl_h_
#endif
#if defined(INLINE_MODULES) || defined(GLCORELOADER_INLINE)
#define )EOM" << cn << "_fq inline\n" <<
"#define " << cn << "_fd inline\n" <<
"#define " << cn << "_vdi(t, n, i) inline t n (i)\n" <<
"#define " << cn << "_vq inline\n" <<
"#elif defined(GLCORELOADER_UNITS)\n" <<
"#define " << cn << "_fq\n" <<
"#define " << cn << "_fd\n" <<
"#define " << cn << "_vdi(t, n, i) t n (i)\n" <<
"#define " << cn << "_vq\n" <<
"#else\n" <<
"#define " << cn << "_fq\n" << // shound this be extern?
"#define " << cn << "_fd\n" <<
"#define " << cn << "_vdi(t, n, i) extern t n\n" <<
"#define " << cn << "_vq extern\n" <<
"#endif" <<
R"EOM(
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#elif defined (__APPLE__)
#include <dlfcn.h>
#else
#include <dlfcn.h>
#include <GL/glx.h>
#endif
)EOM" <<
		"typedef void (*" << cn << "_proc_pointer_t)(void);\n" <<
		"struct " << cn << "_proc_state_t {\n";
	*strout << R"EOM(#ifdef _WIN32
	HMODULE libgl = NULL;
	#elif defined(__APPLE__)
	void * libgl = nullptr;
	#else
	void * libgl = nullptr;
	PFNGLXGETPROCADDRESSPROC glx_get_proc_address = nullptr;
	#endif
)EOM";
	} else { // else of: !opt_lib && !opt_inl
		if (opt_lib) {
			*strout << "#define GLCORELOADER_UNITS\n#include <" << libheadername << ">\n\n";
			goto do_exit; // done
		}

	}

	if (!opt_lib && !opt_inl) {
		if (opt_global) {
			*strout << "\t" << cn << "_fq void initialize(int,int);\n";
		} else {
			*strout << "\t" << cn << "_fq " << cn << "_proc_state_t(int,int);\n";
		}
		*strout << "\t" << cn << "_fq ~" << cn << "_proc_state_t();\n";
		*strout << "\t" << cn << "_fq " << "void open_libgl();\n";
		*strout << "\t" << cn << "_fq " << "void close_libgl();\n";
		*strout << "\t" << cn << "_fq " << cn << "_proc_pointer_t getprocaddr_libgl(const char *);\n";
		if (opt_global) {
			for (std::string& s : gl_function_name_list) {
				*strout << "\tPFN" << string_uppercase(s) << "PROC " << string_rmglpre(s) << " = nullptr;\n";
			}
		} else {
			for (std::string& s : gl_function_name_list) {
				*strout << "\tPFN" << string_uppercase(s) << "PROC " << s << " = nullptr;\n";
			}
		}
		*strout << "\tint " << cn << "_reported_major_version = 0, " << cn << "_reported_minor_version = 0;\n";
		*strout << "};\n";
		if (opt_global) {
			*strout << cn << "_vq " << cn << "_proc_state_t " << cn << "_globl;\n";
			for (std::string& s : gl_function_name_list) {
				*strout << "#define " << s << " " << cn << "_globl." << string_rmglpre(s) << "\n";
			}
			*strout << "#define " << cn << "_initialize(ma, mi) " << cn << "_globl.initialize(ma, mi)\n";
			*strout << "#define " << cn << "_shutdown() " << cn << "_globl.close_libgl()\n";
		}
	}

	if (opt_inl) {
		*strout << "#ifdef _WIN32\n";
		*strout << cn << "_fq " << "void " << fpre << R"EOM(open_libgl()
	libgl = LoadLibraryA("opengl32.dll");
	if (!libgl) throw "Failed to open opengl32.dll";
}
)EOM" << cn << "_fq void " << fpre << R"EOM(close_libgl() {
		FreeLibrary(libgl); libgl = NULL;
}
)EOM" << cn << "_fq " << cn << "_proc_pointer_t " << fpre << "getprocaddr_libgl(const char * s) {\n" <<
"		" << cn << "_proc_pointer_t p = (" << cn << "_proc_pointer_t)wglGetProcAddress(s);\n" <<
"		if (!p) p = (" << cn << "_proc_pointer_t)GetProcAddress(libgl, s);\n" <<
"		return p;\n" <<
"	}\n" <<
"#elif defined (__APPLE__)\n" <<
cn << "_fq void " << fpre << R"EOM(open_libgl() {
		libgl = dlopen("/System/Library/Frameworks/OpenGL.framework/OpenGL", RTLD_LAZY | RTLD_GLOBAL);
		if (!libgl) throw "Failed to open OpenGL library";
	}
	)EOM" << cn << "_fq void " << fpre << R"EOM(close_libgl() {
		dlclose(libgl); libgl = nullptr;
	}
)EOM" << "	" << cn << "_fq " << cn << "_proc_pointer_t " << fpre << "getprocaddr_libgl(const char * s) {\n" <<
"		" << cn << "_proc_pointer_t p;\n" <<
R"EOM(		*(void **)(&p) = dlsym(libgl, s);
		return p;
	}
#else
)EOM" <<
		cn << "_fq void " << fpre << R"EOM(open_libgl() {
		libgl = dlopen("libGL.so", RTLD_LAZY | RTLD_GLOBAL);
		if (!libgl) throw "Failed to open LibGL.so";
		*(void **)(&glx_get_proc_address) = dlsym(libgl, "glXGetProcAddressARB");
	}
)EOM" << cn << "_fq void " << fpre << R"EOM(close_libgl() {
		dlclose(libgl); libgl = nullptr;
	}
)EOM" << cn << "_fq " << cn << "_proc_pointer_t " << fpre << "getprocaddr_libgl(const char * s) {\n" <<
"		" << cn << "_proc_pointer_t p = glx_get_proc_address((const GLubyte *)s);\n" <<
R"EOM(		if (!p) *(void **)(&p) = dlsym(libgl, s);
		return p;
	}
#endif
)EOM";
		if (opt_global) {
			*strout << cn << "_fq void " << fpre << "initialize(int in_major, int in_minor) {\n";
		} else {
			*strout << cn << "_fq " << fpre << cn << "_proc_state_t(int in_major, int in_minor) {\n";
		}
		*strout << R"EOM(	//if (in_major < 3) throw "OpenGL versions below 3 are not supported";
	open_libgl();
)EOM";
		if (opt_global) {
			for (std::string& s : gl_function_name_list) {
				*strout << "\t" << string_rmglpre(s) << " = (PFN" << string_uppercase(s) << "PROC)getprocaddr_libgl(\"" << s << "\");\n";
			}
		} else {
			for (std::string& s : gl_function_name_list) {
				*strout << "\t" << s << " = (PFN" << string_uppercase(s) << "PROC)getprocaddr_libgl(\"" << s << "\");\n";
			}
		}
		*strout << "	if (!glGetIntegerv) {\n" <<
		"		/*throw \"glGetIntegerv missing\";*/\n" <<
		"		if (!glGetString) throw \"Both glGetIntegerv and glGetString appear to be missing.\";\n" <<
		"		const GLubyte * gl_version_string = glGetString(GL_VERSION);\n" <<
		"		if (!gl_version_string) throw \"glGetString returned null, initializing without an opengl context?\";" <<
		R"EOM(
		{
			const GLubyte * p = gl_version_string;
			long vernum[2] = {0, 0};
			size_t verpos = 0;
			size_t strl = 0;
			long cnum = 0;
			for (strl = 0; strl < 1024; strl++) if (p[strl]) continue;
			for (size_t i = 0; i < strl; i++) {
				if (p[i] == '.') {
					vernum[verpos] = cnum;
					verpos++;
					if (verpos >= 2) break;
					cnum = 0;
				} else if (p[i] >= '0' && p[i] <= '9') {
					cnum *= 10;
					cnum += p[i] - '0';
				}
			}
)EOM" <<
		"			" << cn << "_reported_major_version = vernum[0];\n" <<
		"			" << cn << "_reported_minor_version = vernum[1];\n"
		"		}\n" <<
		"	} else {\n" <<
"		glGetIntegerv(GL_MAJOR_VERSION, &" << cn << "_reported_major_version);\n" <<
"		glGetIntegerv(GL_MINOR_VERSION, &" << cn << "_reported_minor_version);\n" <<
		"	}" <<
"		if (in_major > 2) { // if using older opengl, don't check version\n" <<
"			if (" << cn << "_reported_major_version < 3) throw \"OpenGL versions below 3 are not supported\";\n" <<
"			if (" << cn << "_reported_major_version == in_major) {\n" <<
"				if (" << cn << "_reported_minor_version < in_minor) throw \"Requested OpenGL version higher than reported version.\";\n" <<
"			} else if (" << cn << "_reported_major_version < in_major) {\n" <<
"				throw \"Requested OpenGL version higher than reported version.\";\n" <<
"			}\n" <<
"		}\n" <<
"	}\n" <<
cn << "_fq " << fpre << "~" << cn << "_proc_state_t() {\n" <<
"	if (libgl) close_libgl();\n" <<
"}\n";
	} // opt_inl, the big one 
	if (!opt_inl && !opt_lib) {
		*strout << "#if defined(GLCORELOADER_INLINE) || defined(GLCORELOADER_UNITS)\n" <<
			"#include <" << libheadername << ">\n" <<
			"#endif\n";
		*strout << "#endif // __" << cn << "_c_h\n";
	}

	do_exit:
	if (!output_stdout) {
		delete outstream_ptr;
	}
	return 0;
}
