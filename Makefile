#Copyright 2018 Jessica Allan
#
#Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
#
#THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

GLCORELOADER_LIBVER=1.0.0
GLCORELOADER:=$(dir $(realpath $(lastword $(MAKEFILE_LIST))))
# might look scary, but you only depend on the version you need in the outer project
GLCORELOADER_ALL_HEADERS:= \
	$(GLCORELOADER)glcoreloader.h \
	$(GLCORELOADER)glcoreloader_ext.h \
	$(GLCORELOADER)glcoreloader_g.h \
	$(GLCORELOADER)glcoreloader_ext_g.h \
	$(GLCORELOADER)glcoreloader_inl.h \
	$(GLCORELOADER)glcoreloader_ext_inl.h \
	$(GLCORELOADER)glcoreloader_g_inl.h \
	$(GLCORELOADER)glcoreloader_ext_g_inl.h \

GLCORELOADER_ALL_LIBRARIES:= \
	$(GLCORELOADER)glcoreloader.a \
	$(GLCORELOADER)glcoreloader_ext.a \
	$(GLCORELOADER)glcoreloader_g.a \
	$(GLCORELOADER)glcoreloader_ext_g.a \
	$(GLCORELOADER)glcoreloader.so.$(GLCORELOADER_LIBVER) \
	$(GLCORELOADER)glcoreloader_ext.so.$(GLCORELOADER_LIBVER) \
	$(GLCORELOADER)glcoreloader_g.so.$(GLCORELOADER_LIBVER) \
	$(GLCORELOADER)glcoreloader_ext_g.so.$(GLCORELOADER_LIBVER) \

GLCORELOADER_FILES:= \
	$(GLCORELOADER_ALL_HEADERS) \
	$(GLCORELOADER_ALL_LIBRARIES) \
	$(GLCORELOADER)glcoreloader.cxx \
	$(GLCORELOADER)glcoreloader_ext.cxx \
	$(GLCORELOADER)glcoreloader_g.cxx \
	$(GLCORELOADER)glcoreloader_ext_g.cxx \
	$(GLCORELOADER)glcoreloader.o \
	$(GLCORELOADER)glcoreloader_ext.o \
	$(GLCORELOADER)glcoreloader_g.o \
	$(GLCORELOADER)glcoreloader_ext_g.o \
	$(GLCORELOADER)gen_glcoreloader_header


ifndef GLCORELOADER_GLCOREARB_H
GLCORELOADER_GLCOREARB_H:=/usr/include/GL/glcorearb.h
endif
ifndef GLCORELOADER_KHRPLATFORM_H
GLCORELOADER_KHRPLATFORM_H:=/usr/include/KHR/khrplatform.h
endif

ifndef CXX
CXX:=g++
endif
ifndef CXXFLAGS
CXXFLAGS:=-std=c++17 -Wall -Werror -Wshadow -Wformat -Wformat-security -pedantic -ffinite-math-only -fno-signed-zeros -fno-trapping-math -O3 -grecord-gcc-switches -march=native -mtune=native
ifdef BUILD_TYPE
ifeq ($(BUILD_TYPE),DEBUG)
CXXFLAGS:=-std=c++17 -g -Wall -Werror -Wshadow -Wformat -Wformat-security -pedantic -ffinite-math-only -fno-signed-zeros -fno-trapping-math -Og -grecord-gcc-switches
endif
ifeq ($(BUILD_TYPE),RELEASE)
CXXFLAGS:=-std=c++17 -Wall -Werror -Wshadow -Wformat -Wformat-security -pedantic -ffinite-math-only -fno-signed-zeros -fno-trapping-math -O3 -grecord-gcc-switches -march=ivybridge -mtune=skylake
endif
endif
ifneq ($(CXX),clang++)
# clang apparently is too eager with flto? makes unusable binaries
CXXFLAGS:=$(CXXFLAGS) -flto
endif
ifdef LDFLAGS
ifneq (,$(findstring -fpie,$(LDFLAGS)))
CXXFLAGS:=$(CXXFLAGS) -fPIC
endif
endif
endif
ifndef LDFLAGS
LDFLAGS:=$(CXXFLAGS) -pie
GLCORELOADER_LIBRARY_LINK_FLAGS:=$(CXXFLAGS)
CXXFLAGS:=$(CXXFLAGS) -fPIC
endif

# you probably want to instead do this in the outer makefile with only the specific version want
#ifeq ($(CXX),g++)
#AR:=gcc-ar
#GLCORELOADER_FILES:=$(GLCORELOADER_FILES) \
#	$(GLCORELOADER)glcoreloader.h.gch \
#	$(GLCORELOADER)glcoreloader_ext.h.gch \
#	$(GLCORELOADER)glcoreloader_g.h.gch \
#	$(GLCORELOADER)glcoreloader_ext_g.h.gch
#endif

ifeq ($(CXX),g++)
ifneq (,$(findstring -flto,$(CXXFLAGS)))
AR:=gcc-ar
endif
endif

ifeq ($(lastword $(MAKEFILE_LIST)),$(firstword $(MAKEFILE_LIST)))
ARCH:=$(shell uname -m)
ifeq ($(ARCH),x86_64)
LIBDIR:=/lib64
else
LIBDIR:=/lib
endif
ifndef PREFIX
PREFIX:=/usr
endif
.PHONY: clean info install install-user uninstall srctarxz
all: $(GLCORELOADER_FILES)
clean:
	rm -v $(GLCORELOADER_FILES)
info:
	@echo CXXFLAGS:=$(CXXFLAGS)
	@echo LDFLAGS:=$(LDFLAGS)
	@echo GLCORELOADER_LIBRARY_LINK_FLAGS:=$(GLCORELOADER_LIBRARY_LINK_FLAGS)
	@echo GLCORELOADER:=$(GLCORELOADER)
	@echo GLCORELOADER_FILES:=$(GLCORELOADER_FILES)
	@echo BUILD_TYPE:=$(BUILD_TYPE)
	@echo PREFIX:=$(PREFIX)
install-user: PREFIX=$(HOME)/.local
install-user: install
install: $(GLCORELOADER_ALL_HEADERS) $(GLCORELOADER_ALL_LIBRARIES)
	install -m 644 $(GLCORELOADER_ALL_HEADERS) "$(PREFIX)/include"
	install -m 644 $(GLCORELOADER_ALL_LIBRARIES) "$(PREFIX)$(LIBDIR)"
uninstall:
	rm -v $(addprefix $(PREFIX)/include,$(GLCORELOADER_ALL_HEADERS:$(GLCORELOADER)%=/%)) $(addprefix $(PREFIX)$(LIBDIR),$(GLCORELOADER_ALL_LIBRARIES:$(GLCORELOADER)%=/%))
srctarxz: 
	tar -v -c -J -f $(GLCORELOADER)`date +%Y%m%d-%H%M%S`-glcoreloader-$(GLCORELOADER_LIBVER).tar.xz *.cxx Makefile
endif

$(GLCORELOADER)%.h.gch: $(GLCORELOADER)%.h
	$(CXX) -c $(LDFLAGS) $^ -o $@
$(GLCORELOADER)%.o: $(GLCORELOADER)%.cxx
	$(CXX) -c -I./ $(CXXFLAGS) $^ -o $@
$(GLCORELOADER)%.a: $(GLCORELOADER)%.o
	$(AR) rcs $@ $^
$(GLCORELOADER)%.so.$(GLCORELOADER_LIBVER): $(GLCORELOADER)%.o
	$(CXX) $(GLCORELOADER_LIBRARY_LINK_FLAGS) -shared $^ -o $@

# generator

$(GLCORELOADER)gen_glcoreloader_header: $(GLCORELOADER)gen_glcoreloader.cxx
	$(CXX) $(CXXFLAGS) $(GLCORELOADER)gen_glcoreloader.cxx $(LDFLAGS) -o $(GLCORELOADER)gen_glcoreloader_header

# inline/unit headers

$(GLCORELOADER)glcoreloader.h: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H)
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --library-header glcoreloader_inl.h

$(GLCORELOADER)glcoreloader_ext.h: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H)
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --ext --library-header glcoreloader_ext_inl.h

$(GLCORELOADER)glcoreloader_g.h: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H)
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --global --library-header glcoreloader_g_inl.h

$(GLCORELOADER)glcoreloader_ext_g.h: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H)
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --ext --global --library-header glcoreloader_ext_g_inl.h

# inline-library

$(GLCORELOADER)glcoreloader_inl.h: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H)
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --inl

$(GLCORELOADER)glcoreloader_ext_inl.h: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H)
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --ext --inl

$(GLCORELOADER)glcoreloader_g_inl.h: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H)
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --global --inl

$(GLCORELOADER)glcoreloader_ext_g_inl.h: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H)
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --ext --global --inl

# library

$(GLCORELOADER)glcoreloader.cxx: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H) $(GLCORELOADER)glcoreloader_inl.h
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --library --library-header glcoreloader.h

$(GLCORELOADER)glcoreloader_ext.cxx: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H) $(GLCORELOADER)glcoreloader_ext_inl.h
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --ext --library --library-header glcoreloader_ext.h

$(GLCORELOADER)glcoreloader_g.cxx: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H) $(GLCORELOADER)glcoreloader_g_inl.h
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --global --library --library-header glcoreloader_g.h

$(GLCORELOADER)glcoreloader_ext_g.cxx: $(GLCORELOADER)gen_glcoreloader_header $(GLCORELOADER_GLCOREARB_H) $(GLCORELOADER_KHRPLATFORM_H) $(GLCORELOADER)glcoreloader_ext_g_inl.h
	$(GLCORELOADER)gen_glcoreloader_header --input $(GLCORELOADER_GLCOREARB_H) --output $@ --ext --global --library --library-header glcoreloader_ext_g.h
